var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser  = require('body-parser');
var path = require('path');

var pg = require('pg');
var urlUsuarios = "postgres://docker:docker@localhost:5432/docker";
var clientePostgres = new pg.Client(urlUsuarios);

app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    next();
});
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

var requestjson = require('request-json');

var mongoClient = require('mongodb').MongoClient;
var url = "mongodb://mongodb:27017/local";


/*var urlMovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos";
var apiKey = "?apiKey=6mdD839mdDkI1n7nAmYte_G35--UlYBd";*/

app.get('/movimientos', function(req, res) {
/*    var client = requestjson.createClient(urlMovimientosMlab + apiKey);
    client.get('', function(err, resM, body) {
        if (err) {
            console.log(body);
        } else {
            res.send(body);
        }
    });*/
    mongoClient.connect(url, function(err, db) {
        if (err) {
            console.log(err);
        } else {
            console.log("Connected successfully to server");
            var col = db.collection('movimientos');
            col.find({}).limit(3).toArray(function(err, docs) {
                res.send(docs);
            });
        }
        db.close();
    });
});

app.post('/movimientos', function(req, res) {
    var client = requestjson.createClient(urlMovimientosMlab + apiKey);
    client.post('', req.body, function(err, resM, body) {
        if (err) {
            console.log(body);
        } else {
            res.send(body);
        }
    });
});

app.delete('/movimientos/:id', function(req, res) {
    var client = requestjson.createClient(urlMovimientosMlab + "/" + req.params.id + apiKey);
    client.delete('', function(err, resM) {
        if (err) {
            console.log(err);
        } else {
            res.status(resM.statusCode).send(resM.body);
        }
    })
});

app.put('/movimientos/:id', function(req, res) {
    var query = "&q={\"_id\":\"" + req.params.id + "\"}";
    console.log(query);
    var client = requestjson.createClient(urlMovimientosMlab + apiKey + query);
    var body = { "$set": req.body};
    console.log(body);
    client.put('', body, function(err, resM, body) {
        if (err) {
            console.log(body);
        } else {
            res.send(body);
        }
    });
});

app.post('/login', function(req, res) {
    clientePostgres.connect();
    clientePostgres.query("SELECT COUNT(*) FROM usuarios WHERE login=$1 AND password=$2;", [req.body.login, req.body.password], (err, result) => {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            if (result.rows[0].count >= 1) {
                res.send("Login correcto");
            } else {
                res.send("Login incorrecto");
            }
        }
    });

});
